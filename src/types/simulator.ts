export const RandomSimulatorTypes = {
  NORMAL: "normal",
} as const;

export const PeriodicSimulatorTypes = {
  SAWTOOTH: "sawtooth",
  SINE: "sine",
  SQUARE: "square",
  TRIANGLE: "triangle",
} as const;

export type RandomSimulatorType = (typeof RandomSimulatorTypes)[keyof typeof RandomSimulatorTypes];
export type PeriodicSimulatorType = (typeof PeriodicSimulatorTypes)[keyof typeof PeriodicSimulatorTypes];
export type SimulatorType = RandomSimulatorType | PeriodicSimulatorType;
