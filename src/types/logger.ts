export const LoggerLevels = {
  ERROR: "error",
  WARN: "warn",
  INFO: "info",
  DEBUG: "debug",
  TRACE: "trace",
} as const;

export type LoggerLevel = (typeof LoggerLevels)[keyof typeof LoggerLevels];

export const LoggerLevelPriorities = {
  error: 0,
  warn: 10,
  info: 20,
  debug: 30,
  trace: 40,
} as const;

export type LoggerLevelPriority = (typeof LoggerLevelPriorities)[keyof typeof LoggerLevelPriorities];

export const LoggerLevelColors = {
  error: "red",
  warn: "yellow",
  info: "green",
  debug: "cyan",
  trace: "gray",
} as const;

export type LoggerLevelColor = (typeof LoggerLevelColors)[keyof typeof LoggerLevelColors];
