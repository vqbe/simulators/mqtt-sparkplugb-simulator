import "winston-daily-rotate-file";
import winston from "winston";
import { LoggerLevelColors, LoggerLevelPriorities, LoggerLevels } from "../types/logger";
import config from "../config";

winston.addColors(LoggerLevelColors);

const winstonLogger = winston.createLogger({
  levels: LoggerLevelPriorities,
});

const trace = (message: string, ...args: any[]) => {
  winstonLogger.log(LoggerLevels.TRACE, message, ...args);
};

const debug = (message: string, ...args: any[]) => {
  winstonLogger.log(LoggerLevels.DEBUG, message, ...args);
};

const info = (message: string, ...args: any[]) => {
  winstonLogger.log(LoggerLevels.INFO, message, ...args);
};

const warn = (message: string, ...args: any[]) => {
  winstonLogger.log(LoggerLevels.WARN, message, ...args);
};

const error = (message: string, ...args: any[]) => {
  winstonLogger.log(LoggerLevels.ERROR, message, ...args);
};

const consoleLoggerFormat = winston.format.combine(
  winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss.SSS" }),
  winston.format.colorize(),
  winston.format.splat(),
  winston.format.printf((log) => `${log.timestamp} ${log.level}\t${log.message}`)
);
const consoleLogger = new winston.transports.Console({
  level: config.logger.console.level,
  format: consoleLoggerFormat,
});
winstonLogger.add(consoleLogger);

if (config.logger.file.enabled) {
  const dailyRotateFileFormat = winston.format.combine(
    winston.format.timestamp({ format: "YYYY-MM-DD HH:mm:ss.SSS" }),
    winston.format.splat(),
    winston.format.printf((log) => `${log.timestamp} ${log.level}\t${log.message}`)
  );
  const dailyRotateFileTransport = new winston.transports.DailyRotateFile({
    level: config.logger.file.level,
    format: dailyRotateFileFormat,
    dirname: config.logger.file.baseDir,
    filename: `${config.logger.file.filenamePrefix}-%DATE%.log`,
    datePattern: config.logger.file.datePattern,
    maxSize: config.logger.file.maxSize,
    maxFiles: config.logger.file.maxFiles,
    zippedArchive: config.logger.file.zippedArchive,
  });
  dailyRotateFileTransport.on("rotate", (oldFilename, newFilename) => {
    info(`Logger file rotated: ${oldFilename} -> ${newFilename}`);
  });
  winstonLogger.add(dailyRotateFileTransport);
}

export const logger = {
  trace,
  debug,
  info,
  warn,
  error,
};
