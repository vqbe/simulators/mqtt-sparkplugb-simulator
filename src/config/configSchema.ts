import { z } from "zod";
import { LoggerLevels } from "../types/logger";
import namespaceSchema from "./simulator/namespaceSchema";

export const configSchema = z.object({
  sparkplug_b: z.object({
    namespaces: z.array(namespaceSchema).default([]),
  }),
  logger: z.object({
    console: z.object({
      level: z.nativeEnum(LoggerLevels),
    }),
    file: z.object({
      enabled: z.boolean(),
      level: z.nativeEnum(LoggerLevels).default(LoggerLevels.INFO),
      baseDir: z.string().default("./logs"),
      filenamePrefix: z.string().default("log"),
      datePattern: z.string().default("YYYY-MM-DD"),
      maxSize: z
        .number()
        .int()
        .positive()
        .default(1024 * 1024 * 10),
      maxFiles: z.number().int().positive().default(5),
      zippedArchive: z.boolean().default(false),
    }),
  }),
});

export default configSchema;
export type Config = z.infer<typeof configSchema>;
