import path from "path";
import yaml from "yaml";

import schema from "./configSchema";
import { readFileSync } from "fs";

const configPath = path.join(process.cwd(), "config", "config.yaml");
const configFile = readFileSync(configPath, "utf8");
const yamlConfig = yaml.parse(configFile);

const config = schema.safeParse(yamlConfig);

if (!config.success) {
  console.error("Config validation error:");
  for (const error of config.error.errors) {
    console.error(`- ${error.path.join(".")} :: ${error.message}`);
  }
  process.exit(1);
  // throw new Error(`Config validation error: ${config.error.message}`);
}

export default config.data;
