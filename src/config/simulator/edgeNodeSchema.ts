import { z } from "zod";
import deviceSchema from "./deviceSchema";

export default z.object({
  edge_node_id: z.string(),
  devices: z.array(deviceSchema),
});
