import { z } from "zod";
import { RandomSimulatorTypes, PeriodicSimulatorTypes } from "../../types/simulator";

const simulatorBaseSchema = z.object({
  metric_name: z.string(),
});

const randomSimulatorSchema = z.object({
  simulator_type: z.enum([RandomSimulatorTypes.NORMAL]),
  mean: z.number(),
  standard_deviation: z.number().positive(),
});

const periodicSimulatorSchema = z
  .object({
    simulator_type: z.enum([
      PeriodicSimulatorTypes.SAWTOOTH,
      PeriodicSimulatorTypes.SINE,
      PeriodicSimulatorTypes.SQUARE,
      PeriodicSimulatorTypes.TRIANGLE,
    ]),
    min: z.number(),
    max: z.number(),
    period: z.number().positive(),
  })
  .refine((data) => data.min < data.max, {
    message: "min must be less than max",
    path: ["min"],
  });

export default simulatorBaseSchema.and(randomSimulatorSchema.or(periodicSimulatorSchema));
