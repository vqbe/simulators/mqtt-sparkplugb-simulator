import { z } from "zod";
import simulatorSchema from "./simulatorSchema";

export default z.object({
  device_id: z.string(),
  publish_interval: z.number().positive().min(100),
  simulators: z.array(simulatorSchema),
});
