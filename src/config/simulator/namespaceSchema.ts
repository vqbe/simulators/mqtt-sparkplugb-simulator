import { z } from "zod";
import edgeNodeSchema from "./edgeNodeSchema";

export default z.object({
  group_id: z.string(),
  edge_nodes: z.array(edgeNodeSchema),
});
