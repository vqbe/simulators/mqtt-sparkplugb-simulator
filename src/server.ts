import { logger } from "./utils/logger";

const LOGGER_PREFIX = "server";

logger.info(`${LOGGER_PREFIX} :: 📦 App version: ${process.env.npm_package_version}`);
logger.info(`${LOGGER_PREFIX} :: 🟩 Node version: ${process.versions.node}`);
logger.info(`${LOGGER_PREFIX} :: 📁 current file dir: ${__dirname}`);
logger.info(`${LOGGER_PREFIX} :: 📁 current work dir: ${process.cwd()}`);
logger.info(`${LOGGER_PREFIX} :: 🏭 starting... process pid: <${process.pid}>`);
